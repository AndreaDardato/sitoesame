const express = require("express");
const mysql = require("mysql");
const bodyParser = require("body-parser");


var temp = [];


//create connection
const DB = mysql.createConnection({
    host: '127.0.0.1',
    user: 'andrea',
    password: '',
    database: 'METEO'
});

DB.connect((err) => {
    if(err) {
        throw err;
    }
    console.log("MySQL connected")
});

const app = express();

//confifuration
//app.use(express.static("siteFE"));
app.set("view engine", "jade");
app.use('/bootstrap', express.static(__dirname + "/node_modules/bootstrap/dist/css/"))
//app.set("view engine", 'html')

/*SEZIONE DI ROUTE DEL SITO*/
app.get('/test', (req, res) => {
    let query = "select * FROM Station WHERE dateAndTime = (SELECT max(dateAndTime) FROM Station);"
    DB.query(query, (err, result) => {
        if(err) {
            throw err;
        }
        console.log(result);
        res.send("It works fine");
    })
});

app.get('/prova', (req, res) => {
    //res.writeHead(200, {'Content-Type': 'application/json'})
    let queryTest = "select * FROM Station limit 2;"
    var myData;
    DB.query(queryTest, (err, result) => {
        if(err) {
            throw err;
        }
        console.log(result);
        // res.end(JSON.stringify(result))
        //res.render('lookNow', {'test': result});
    })
    
});

app.get('/seconda', (req, res) => {

    let getDateAndTime = "SELECT Temperatura FROM Station WHERE dateAndTime = (SELECT max(dateAndTime) FROM Station)"
    DB.query(getDateAndTime, (err, result) => {
        if(err) {
            throw err;
        }
        console.log("result: ", result);

        //var dateAndTiems = [];

        //console.log("cerco di vedere la temperatura: ", result[0][2])

        // for(var i = 0; i < result.length; i++) {
        //     var currentDate = result[i];
        //     var data = currentDate.dateAndTime;
        //     dateAndTiems.push(data);
        // }

        for(var i = 0; i < result.length; i++) {
            var currentTemp = result[i];
            var temperatura = currentTemp.Temperatura;
            temp.push(temperatura);
        }

        console.log(temp)

        res.render('lookNow', {
            "temperature": temp,
        });

    });

})

app.get('/altroTest', (req, res) => {
    let queryAltroTest = "select * FROM Station WHERE dateAndTime = (SELECT max(dateAndTime) FROM Station);"
    DB.query(queryAltroTest, (err, result) => {
        if(err) {
            throw err;
        }

        console.log(result);

        //result = JSON.stringify(result);

        dateAndTime = result[0].dateAndTime;
        dataOdierna = result[0].DataOdierna;
        ora = result[0].Ora;
        temperatura = result[0].Temperatura;
        umidita = result[0].Umidita;
        pressione = result[0].Pressione;

        // console.log("date and time: ", dateAndTime);
        // console.log("data odierna: ", dataOdierna);

        res.render('lookNow', {
            "dateAndTime": dateAndTime,
            "dataOdierna": dataOdierna,
            "ora": ora,
            "temperatura": temperatura,
            "umidita": umidita,
            "pressione": pressione,
        });

    })
})



/*METTO IN ASCOLTO IL SERVER SULLA PORTA 3000*/
app.listen(3000, () => {
    console.log("Server started at port 3000")
})